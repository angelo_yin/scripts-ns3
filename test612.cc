/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/mmwave-helper.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store.h"
#include "ns3/mmwave-helper.h"
#include "ns3/log.h"
#include "ns3/mmwave-point-to-point-epc-helper.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/eps-bearer-tag.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/test.h"
#include "ns3/abort.h"
#include "ns3/object.h"

using namespace ns3;

int main(int argc, char **argv)
{
    // LogComponentEnable("MmWaveMacSchedulerUeInfoPF",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveMacSchedulerTdmaPF",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveMacSchedulerTdmaRR",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveMacSchedulerTdmaMR",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveMacSchedulerNs3",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("LteRlcAm",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("LteRlcUm",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("LteEnbRrc",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveEnbMac",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveEnbNetDevice",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveEnbPhy",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWavePhy",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWave3gppChannel",LOG_LEVEL_FUNCTION);
    // LogComponentEnable("MmWaveMacSchedulerUeInfoPF",LOG_LEVEL_INFO);

    // //MmWaveEnbPhy

    // LogComponentEnableAll(LOG_LEVEL_FUNCTION);

    uint32_t numerology = 5; // the numerology to be used
    uint32_t rb = 60;
    double bw1 = 347.83e6;      // bandwidth of bandwidth part 1
    bool isDownlink = 1;     // whether to generate the downlink traffic
    bool isUplink = 0;       // whether to generate the uplink traffic
    uint32_t usersNum = 12;   // number of users
    uint32_t beamsNum = 1;   // currently the test is supposed to work with maximum 4 beams per gNb
    std::string schedulerType[] = {"ns3::MmWaveMacSchedulerTdmaRR", "ns3::MmWaveMacSchedulerTdmaPF", "ns3::MmWaveMacSchedulerTdmaMR", "ns3::MmWaveMacSchedulerOfdmaRR", "ns3::MmWaveMacSchedulerOfdmaPF", "ns3::MmWaveMacSchedulerOfdmaMR"};

    // set simulation time and mobility
    double simTime = 0.2;
    uint16_t gNbNum = 1;
    uint32_t packetSize = 1000;
    uint32_t choice = 4;
    std::string dtrt = "100Mbps";
    int distAllocer = 1;
    double delta = 0.2;
    double longlength = 2;
    double distance = 400;
    double fi = 1;
    double power = 20.0;
    std::string scenario="UMa";
    bool sha = false;
    bool beamsearch = false;

    CommandLine cmd;
    cmd.AddValue("simTime", "simTime in [s]", simTime);
    cmd.AddValue("numerology", "the numerology to be used", numerology);
    cmd.AddValue("isDownlink", "whether to generate the downlink traffic", isDownlink);
    cmd.AddValue("isUplink", "whether to generate the uplink traffic", isUplink);
    cmd.AddValue("choice", "mac scheduler choice", choice);
    cmd.AddValue("userNumber", "number of users pre beam", usersNum);
    cmd.AddValue("beamNumber", "number of beams", beamsNum);
    cmd.AddValue("datarate", "datarate", dtrt);
    cmd.AddValue("scenario", "scenario", scenario);
    cmd.AddValue("fi", "fi", fi);
    cmd.AddValue("power", "power", power);
    cmd.AddValue("rb", "rb", rb);
    cmd.AddValue("sha", "sha", sha);
    cmd.AddValue("beamsearch", "beamsearch", beamsearch);


    cmd.AddValue("distance", "Distance between eNBs [m]", distance);
    cmd.AddValue("distAllocer", "distAllocer", distAllocer);
    cmd.AddValue("delta", "delta", delta);
    cmd.AddValue("long", "longlength", longlength);
    cmd.Parse(argc, argv);
    bw1=400e6/69*rb+1e-6;

    DataRate udpRate = DataRate(dtrt);
    Time udpAppStartTimeDl = MilliSeconds(100);
    Time udpAppStartTimeUl = MilliSeconds(100);
    Time udpAppStopTimeDl = Seconds(simTime);
    Time udpAppStopTimeUl = Seconds(simTime);

    Config::SetDefault("ns3::MmWave3gppPropagationLossModel::ChannelCondition", StringValue("l"));
    Config::SetDefault("ns3::MmWave3gppPropagationLossModel::Scenario", StringValue(scenario)); // with antenna height of 10 m  UMi-StreetCanyon
    Config::SetDefault("ns3::MmWave3gppPropagationLossModel::Shadowing", BooleanValue(sha));
    Config::SetDefault("ns3::LteRlcUm::MaxTxBufferSize", UintegerValue(999999999));
    Config::SetDefault("ns3::MmWave3gppChannel::CellScan", BooleanValue(beamsearch));
    Config::SetDefault("ns3::MmWave3gppChannel::BeamSearchAngleStep", DoubleValue(10.0));
    // Config::SetDefault("ns3::MmWave3gppChannel::Blockage", BooleanValue(true));        // use blockage or not
    // Config::SetDefault("ns3::MmWave3gppChannel::PortraitMode", BooleanValue(true));    // use blockage model with UT in portrait mode
    // Config::SetDefault("ns3::MmWave3gppChannel::NumNonselfBlocking", IntegerValue(4)); // number of non-self blocking obstacles
    Config::SetDefault("ns3::MmWaveEnbPhy::TxPower", DoubleValue(power));
    Config::SetDefault("ns3::MmWaveUePhy::TxPower", DoubleValue(power));
    // Config::SetDefault("ns3::MmWaveMacSchedulerNs3::StartingMcsDl", UintegerValue(28));
    // Config::SetDefault("ns3::MmWaveMacSchedulerNs3::StartingMcsUl", UintegerValue(28));
    Config::SetDefault("ns3::EpsBearer::Release", UintegerValue(15));

    Config::SetDefault("ns3::MmWaveMacSchedulerTdmaPF::FairnessIndex", DoubleValue(fi));
    Config::SetDefault("ns3::MmWaveMacSchedulerOfdmaPF::FairnessIndex", DoubleValue(fi));
    // Config::SetDefault("ns3::MmWaveUeNetDevice::AntennaNumDim1", UintegerValue(2));
    // Config::SetDefault("ns3::MmWaveUeNetDevice::AntennaNumDim2", UintegerValue(4));
    // Config::SetDefault("ns3::MmWaveEnbNetDevice::AntennaNumDim1", UintegerValue(4));
    // Config::SetDefault("ns3::MmWaveEnbNetDevice::AntennaNumDim2", UintegerValue(8));

    // Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue(640));

    // Config::SetDefault("ns3::MmWaveEnbPhy::NoiseFigure", DoubleValue(0));
    // Config::SetDefault("ns3::MmWaveUePhy::NoiseFigure", DoubleValue(0));

    Config::SetDefault("ns3::LteEnbRrc::EpsBearerToRlcMapping", EnumValue(LteEnbRrc::RLC_UM_ALWAYS));
    // Config::SetDefault("ns3::LteEnbRrc::EpsBearerToRlcMapping", EnumValue(LteEnbRrc::RLC_UM_ALWAYS));

    // setup the mmWave simulation
    Ptr<MmWaveHelper> mmWaveHelper = CreateObject<MmWaveHelper>(); //SetHarqEnabled
    // mmWaveHelper->SetHarqEnabled(false);
    // std::cout<<mmWaveHelper->GetHarqEnabled()<<std::endl;
    // mmWaveHelper->SetQciOfRnti(1,2);
    // mmWaveHelper->SetQciOfRnti(2,9);
    mmWaveHelper->SetAttribute("PathlossModel", StringValue("ns3::MmWave3gppPropagationLossModel"));
    mmWaveHelper->SetAttribute("ChannelModel", StringValue("ns3::MmWave3gppChannel"));

    Ptr<BandwidthPartsPhyMacConf> bwpConf = CreateObject<BandwidthPartsPhyMacConf>();
    Ptr<MmWavePhyMacCommon> phyMacCommonBwp = CreateObject<MmWavePhyMacCommon>();
    phyMacCommonBwp->SetCentreFrequency(28e9);
    phyMacCommonBwp->SetBandwidth(bw1);
    phyMacCommonBwp->SetNumerology(numerology);
    std::cout << schedulerType[choice] << std::endl;
    phyMacCommonBwp->SetAttribute("MacSchedulerType", TypeIdValue(TypeId::LookupByName(schedulerType[choice])));

    bwpConf->AddBandwidthPartPhyMacConf(phyMacCommonBwp);
    mmWaveHelper->SetBandwidthPartMap(bwpConf);

    Ptr<MmWavePointToPointEpcHelper> epcHelper = CreateObject<MmWavePointToPointEpcHelper>();
    mmWaveHelper->SetEpcHelper(epcHelper);
    mmWaveHelper->Initialize();

    // create base stations and mobile terminals
    NodeContainer gNbNodes;
    NodeContainer ueNodes;
    MobilityHelper mobility;

    double gNbHeight = 25;
    double ueHeight = 1.5;
    if (scenario=="RMa")
    {
        gNbHeight = 35;
        ueHeight = 1.5;
    }
    else if (scenario=="UMa")
    {
        gNbHeight = 25;
        ueHeight = 1.5;
        if(distance<35){
            puts("Distance too small");
        }
    }
    else if (scenario=="UMi-StreetCanyon")
    {
        gNbHeight = 10;
        ueHeight = 1.5;
    }
    else if (scenario=="InH-OfficeMixed" || scenario=="InH-OfficeOpen" || scenario=="InH-ShoppingMall")
    {
        //The fast fading model does not support 'InH-ShoppingMall' since it is listed in table 7.5-6
        gNbHeight = 3;
        ueHeight = 1;
    }
    else
    {
        return 1;
    }
    gNbNodes.Create(gNbNum);
    ueNodes.Create(usersNum * beamsNum * gNbNum);

    Ptr<ListPositionAllocator> apPositionAlloc = CreateObject<ListPositionAllocator>();
    Ptr<ListPositionAllocator> staPositionAlloc = CreateObject<ListPositionAllocator>();

    double gNbx = 0;
    double gNby = 0;

    for (uint gNb = 0; gNb < gNbNum; gNb++)
    {
        apPositionAlloc->Add(Vector(gNbx, gNby, gNbHeight));

        for (uint beam = 1; beam <= beamsNum; beam++)
        {
            for (uint ueBeam = 0; ueBeam < usersNum; ueBeam++)
            {
                double the = 1.0 * ueBeam / usersNum * 2 * 3.1416;
                if (distAllocer == 1)
                {
                    staPositionAlloc->Add(Vector(gNbx + distance * cos(the), gNby + distance * sin(the), ueHeight));
                }
                else if (distAllocer == 2)
                {
                    staPositionAlloc->Add(Vector(gNbx + distance * longlength * cos(the), gNby + distance * sin(the), ueHeight));
                }
                else if (distAllocer == 3)
                {
                    staPositionAlloc->Add(Vector(gNbx + distance, gNby + 0, ueHeight));
                }
                else if (distAllocer == 4)
                {
                    staPositionAlloc->Add(Vector(gNbx + distance + ueBeam * delta, gNby + 0, ueHeight));
                }
                else if (distAllocer == 5)
                {
                    staPositionAlloc->Add(Vector(distance, 0, ueHeight));
                    staPositionAlloc->Add(Vector(distance * 2, 0, ueHeight));
                    staPositionAlloc->Add(Vector(distance * 3, 0, ueHeight));
                }
                else if (distAllocer == 6)
                {
                    switch (ueBeam / (usersNum / 3))
                    {
                    case 0:
                        staPositionAlloc->Add(Vector(gNbx + distance * cos(the), gNby + distance * sin(the), ueHeight));
                        break;
                    case 1:
                        staPositionAlloc->Add(Vector(gNbx + 3 * distance * cos(the), gNby + 3 * distance * sin(the), ueHeight));
                        break;
                    case 2:
                        staPositionAlloc->Add(Vector(gNbx + 5 * distance * cos(the), gNby + 5 * distance * sin(the), ueHeight));
                        break;

                    default:
                        staPositionAlloc->Add(Vector(gNbx + distance * cos(the), gNby + distance * sin(the), ueHeight));
                        break;
                    };
                }
                else
                {
                    staPositionAlloc->Add(Vector(0, 0, 2));
                }
            }
        }
        gNbx += 1000;
        // gNby += 1;
    }
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.SetPositionAllocator(apPositionAlloc);
    mobility.Install(gNbNodes);
    mobility.SetPositionAllocator(staPositionAlloc);
    mobility.Install(ueNodes);
    // install mmWave net devices
    NetDeviceContainer enbNetDev = mmWaveHelper->InstallEnbDevice(gNbNodes);
    NetDeviceContainer ueNetDev = mmWaveHelper->InstallUeDevice(ueNodes);
    // create the internet and install the IP stack on the UEs
    // get SGW/PGW and create a single RemoteHost
    Ptr<Node> pgw = epcHelper->GetPgwNode();
    NodeContainer remoteHostContainer;
    remoteHostContainer.Create(1);
    Ptr<Node> remoteHost = remoteHostContainer.Get(0);
    InternetStackHelper internet;
    internet.Install(remoteHostContainer);
    // connect a remoteHost to pgw. Setup routing too
    PointToPointHelper p2ph;
    p2ph.SetDeviceAttribute("DataRate", DataRateValue(DataRate("100Gb/s")));
    p2ph.SetDeviceAttribute("Mtu", UintegerValue(2500));
    p2ph.SetChannelAttribute("Delay", TimeValue(Seconds(0.000)));
    NetDeviceContainer internetDevices = p2ph.Install(pgw, remoteHost);
    Ipv4AddressHelper ipv4h;
    ipv4h.SetBase("1.0.0.0", "255.0.0.0");
    Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign(internetDevices);
    // in this container, interface 0 is the pgw, 1 is the remoteHost
    Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress(1);

    Ipv4StaticRoutingHelper ipv4RoutingHelper;
    Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting(remoteHost->GetObject<Ipv4>());
    remoteHostStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.0.0.0"), 1);
    internet.Install(ueNodes);
    Ipv4InterfaceContainer ueIpIface;
    ueIpIface = epcHelper->AssignUeIpv4Address(NetDeviceContainer(ueNetDev));

    // Set the default gateway for the UEs
    for (uint32_t j = 0; j < ueNodes.GetN(); ++j)
    {
        Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting(ueNodes.Get(j)->GetObject<Ipv4>());
        ueStaticRouting->SetDefaultRoute(epcHelper->GetUeDefaultGatewayAddress(), 1);
    }

    // attach UEs to the closest eNB
    mmWaveHelper->AttachToClosestEnb(ueNetDev, enbNetDev);
    // std::cout<<mmWaveHelper->GetHarqEnabled()<<std::endl;

    // assign IP address to UEs, and install UDP downlink applications
    uint16_t dlPort = 1234;
    uint16_t ulPort = 2000;
    ApplicationContainer clientAppsDl;
    ApplicationContainer serverAppsDl;
    ApplicationContainer clientAppsUl;
    ApplicationContainer serverAppsUl;
    ObjectMapValue objectMapValue;

    Time udpInterval = Time::FromDouble((packetSize * 8) / static_cast<double>(udpRate.GetBitRate()), Time::S);

    if (isUplink)
    {
        // configure here UDP traffic
        for (uint32_t j = 0; j < ueNodes.GetN(); ++j)
        {
            UdpServerHelper ulPacketSinkHelper(ulPort);
            serverAppsUl.Add(ulPacketSinkHelper.Install(remoteHost));

            UdpClientHelper ulClient(remoteHostAddr, ulPort);
            ulClient.SetAttribute("MaxPackets", UintegerValue(0xFFFFFFFF));
            ulClient.SetAttribute("PacketSize", UintegerValue(packetSize));
            ulClient.SetAttribute("Interval", TimeValue(udpInterval)); // we try to saturate, we just need to measure during a short time, how much traffic can handle each BWP
            clientAppsUl.Add(ulClient.Install(ueNodes.Get(j)));
            ulPort++;
        }

        serverAppsUl.Start(udpAppStartTimeUl);
        clientAppsUl.Start(udpAppStartTimeUl);
        serverAppsUl.Stop(udpAppStopTimeUl);
        clientAppsUl.Stop(udpAppStopTimeUl);
    }

    if (isDownlink)
    {
        UdpServerHelper dlPacketSinkHelper(dlPort);
        serverAppsDl.Add(dlPacketSinkHelper.Install(ueNodes));

        // configure here UDP traffic
        for (uint32_t j = 0; j < ueNodes.GetN(); ++j)
        {
            UdpClientHelper dlClient(ueIpIface.GetAddress(j), dlPort);
            dlClient.SetAttribute("MaxPackets", UintegerValue(0xFFFFFFFF));
            dlClient.SetAttribute("PacketSize", UintegerValue(packetSize));
            dlClient.SetAttribute("Interval", TimeValue(udpInterval)); // we try to saturate, we just need to measure during a short time, how much traffic can handle each BWP
            clientAppsDl.Add(dlClient.Install(remoteHost));
        }
        // start UDP server and client apps
        serverAppsDl.Start(udpAppStartTimeDl);
        clientAppsDl.Start(udpAppStartTimeDl);
        serverAppsDl.Stop(udpAppStopTimeDl);
        clientAppsDl.Stop(udpAppStopTimeDl);
    }
    // mmWaveHelper->EnableTraces();

    // FlowMonitorHelper flowmon;
    // Ptr<FlowMonitor> monitor = flowmon.Install(ueNodes);
    // flowmon.Install(gNbNodes);

    //mmWaveHelper->EnableTraces();
    Simulator::Stop(Seconds(simTime));
    Simulator::Run();

    // monitor->CheckForLostPackets();

    // Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(flowmon.GetClassifier());
    // auto stats = monitor->GetFlowStats();

    // for (auto i = stats.begin(); i != stats.end(); ++i)
    // {
    //     Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);

    //     std::cout << "Flow ID: " << i->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress << std::endl;
    //     std::cout << "Tx Packets = " << i->second.txPackets << std::endl;
    //     std::cout << "Rx Packets = " << i->second.rxPackets << std::endl;
    //     double Throughput = i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds() - i->second.timeFirstTxPacket.GetSeconds()) / 1024;
    //     std::cout << "Throughput: " << Throughput << " Kbps" << std::endl;
    // }

    // MmWaveMacSchedulerNs3::CalcFairnessIndex ();

    double throughputDl = 0;
    double throughputUl = 0;
    if (isDownlink)
    {
        for (uint32_t i = 0; i < serverAppsDl.GetN(); i++)
        {
            Ptr<UdpServer> serverApp1 = serverAppsDl.Get(i)->GetObject<UdpServer>();
            Ptr<UdpClient> clientApp1 = clientAppsDl.Get(i)->GetObject<UdpClient>();

            uint32_t sent = clientApp1->GetSent();
            uint32_t recv = serverApp1->GetReceived();
            uint32_t los = serverApp1->GetLost();

            std::cout << "User " << i + 1 << std::endl;
            std::cout << "Tx " << sent << std::endl;
            std::cout << "Rx " << recv << std::endl;
            std::cout << "Lost " << los << std::endl;

            double throuhgput2 = (serverApp1->GetReceived() * packetSize * 8) / (udpAppStopTimeDl - udpAppStartTimeDl).GetSeconds();
            std::cout << "DL Throughput " << i + 1 << ":" << static_cast<uint32_t>(throuhgput2) << std::endl;
            throughputDl += throuhgput2;
        }
        std::cout << "\n Total DL UDP throughput " << static_cast<uint32_t>(throughputDl / 1e3) << " Kbps" << std::endl;
    }
    // if (isDownlink)
    // {
    //     Ptr<UdpServer> serverApp1 = serverAppsDl.Get(0)->GetObject<UdpServer>();
    //     double throuhgput1 = (serverApp1->GetReceived() * packetSize * 8) / (udpAppStopTimeDl - udpAppStartTimeDl).GetSeconds();
    //     throughputDl = throuhgput1;
    //     std::cout << "DL Throughput 1:" << static_cast<uint32_t>(throughputDl) << std::endl;
    //     for (uint32_t i = 1; i < serverAppsDl.GetN(); i++)
    //     {
    //         Ptr<UdpServer> serverApp2 = serverAppsDl.Get(i)->GetObject<UdpServer>();
    //         double throuhgput2 = (serverApp2->GetReceived() * packetSize * 8) / (udpAppStopTimeDl - udpAppStartTimeDl).GetSeconds();
    //         std::cout << "DL Throughput " << i + 1 << ":" << static_cast<uint32_t>(throuhgput2) << std::endl;
    //         throughputDl += throuhgput2;
    //     }
    //     std::cout << "\n Total DL UDP throughput " << static_cast<uint32_t>(throughputDl) << " bps" << std::endl;
    // }
    if (isUplink)
    {
        Ptr<UdpServer> serverApp1 = serverAppsUl.Get(0)->GetObject<UdpServer>();
        double throuhgput1 = (serverApp1->GetReceived() * packetSize * 8) / (udpAppStopTimeUl - udpAppStartTimeUl).GetSeconds();
        throughputUl = throuhgput1;
        std::cout << "UL Throughput 1:" << static_cast<uint32_t>(throughputUl) << std::endl;
        for (uint32_t i = 1; i < serverAppsUl.GetN(); i++)
        {
            Ptr<UdpServer> serverApp2 = serverAppsUl.Get(i)->GetObject<UdpServer>();
            double throuhgput2 = (serverApp2->GetReceived() * packetSize * 8) / (udpAppStopTimeUl - udpAppStartTimeUl).GetSeconds();
            throughputUl += throuhgput2;
            std::cout << "UL Throughput " << i + 1 << ":" << static_cast<uint32_t>(throuhgput2) << std::endl;
        }
        std::cout << "\n Total UL UDP throughput " << static_cast<uint32_t>(throughputUl) << " bps" << std::endl;
    }

    Simulator::Destroy();
    return 0;
}
